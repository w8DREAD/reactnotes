import React from 'react';

export default class Footer extends React.Component {
    render() {
        const code = '</co'+'de>';
        return <footer>
            <ul>
                <li>
                    <p className="home">Home</p>
                </li>
                <li>
                    <p className="services">Services</p>
                </li>
                <li>
                    <p className="reachus">Reach us</p>
                </li>
                <li>
                    <p className="clients">Clients</p>
                </li>
                <div style={{textAlign: "center"}}>
                    <a style={{fontSize: "18px", textAlign: "center"}}>
                        Данный сайт предназначен для демострации
                        моей стажировки в lodoss-team</a><br></br>
                    <i>In {code} we trust &copy; <br></br> 2019</i>
                </div>
            </ul>
        </footer>
    }
}
