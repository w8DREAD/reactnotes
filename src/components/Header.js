import React from 'react';
import Navi from "./Navi";

export default class Header extends React.Component {
    render() {
        return <header>
            <form style={{width: "100%"}} name="description"><h3 style={{margin: "15px 0 0 50px"}}>Храните ваши заметки
                у нас.
                Легко и просто.</h3></form>
            <form>
                <div className="register-form">
                    <h3>Hey, username! </h3>
                    {/*<h6>Набрано лайков: like</h6>*/}
                    {/*<a href="/logout" className="badge badge-primary" style={{fontSize: "x-large"}}>Logout</a>*/}
                    <a href="/login" className="badge badge-primary" style={{fontSize: "large"}}>войти</a> |
                    <a href="/register" className="badge badge-primary" style={{fontSize: "large"}}>регистрация</a>
                </div>
                <Navi/>
            </form>
        </header>
    }
}
