import { NavLink } from 'react-router-dom';
import React from 'react';
export default class Navi extends React.Component{
    render(){
        return <ul className="pagination pagination-lg" style={{marginLeft: "50px"}}>
            <li className="page-item"><NavLink exact className="page-link" activeClassName="active" to="/">Главная</NavLink></li>
            <li className="page-item"><NavLink exact className="page-link" activeClassName="active" to="/pageNotes">Новости</NavLink></li>
            <li className="page-item"><NavLink exact className="page-link" activeClassName="active" to="/features">Фичи</NavLink></li>
            <li className="page-item"><NavLink exact className="page-link" activeClassName="active" to="/logs">Логи</NavLink></li>
        </ul>;
    }
}
