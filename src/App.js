import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import Main from "./components/Main";
import Notes from "./components/Notes";
import Features from "./components/Features";
import Logs from "./components/Logs";
import Footer from "./components/Footer";

export default function App () {
    return <BrowserRouter>
        <Header />
        <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/pageNotes" component={Notes} />
            <Route exact path="/features" component={Features} />
            <Route exact path="/logs" component={Logs} />
        </Switch>
        <Footer />
    </BrowserRouter>;
}
